# By Carlos Asmat
# Colours inspired by donahut
# https://gist.github.com/donahut/ca6ec737c983c7fe6cd738573aeabd87

#Prompt
# Options
set __fish_git_prompt_showuntrackedfiles 'yes'
set __fish_git_prompt_show_informative_status 'yes'
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate 'yes'
set __fish_git_prompt_showcolorhints
set __fish_git_prompt_showupstream "informative"
set __fish_git_prompt_shorten_branch_len "15"
set __fish_git_prompt_char_upstream_prefix ""

# Colours
set green (set_color green)
set magenta (set_color magenta)
set normal (set_color normal)
set red (set_color red)
set yellow (set_color yellow)

set __fish_git_prompt_color_branch magenta --bold
set __fish_git_prompt_color_dirtystate white
set __fish_git_prompt_color_invalidstate red
set __fish_git_prompt_color_merging yellow
set __fish_git_prompt_color_stagedstate yellow
set __fish_git_prompt_color_upstream_ahead green
set __fish_git_prompt_color_upstream_behind red

# Icons
set __fish_git_prompt_char_cleanstate '✔️ '
set __fish_git_prompt_char_conflictedstate '🔥'
set __fish_git_prompt_char_dirtystate '❌'
set __fish_git_prompt_char_invalidstate '⚠️ '
set __fish_git_prompt_char_stagedstate '🚥'
set __fish_git_prompt_char_stashstate '📦'
set __fish_git_prompt_char_stateseparator '|'
set __fish_git_prompt_char_untrackedfiles '🔎'
set __fish_git_prompt_char_upstream_ahead ' ⏫'
set __fish_git_prompt_char_upstream_behind ' ⏬'
set __fish_git_prompt_char_upstream_diverged ' 🚧'
set __fish_git_prompt_char_upstream_equal ' 💯' 

function fish_prompt --description 'Write out the prompt'

   #echo #space between commands

   set_color cyan
   printf '%s' (hostname|cut -d . -f 1)
   set_color normal
   printf ':'

   set_color $fish_color_cwd
   printf '%s' (prompt_pwd)
   set_color normal

   # Line 2
   #echo
   if test $VIRTUAL_ENV
       printf "(%s)" (set_color blue)(basename $VIRTUAL_ENV)(set_color normal)
   end

   printf '%s' (__fish_git_prompt)

   printf '$ '

   set_color normal
end