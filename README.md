# fish_settings

My fish settings. Includes fortune greeting and git prompt.

## Installation
Clone inside of ~/.config/fish
```bash
cd ~/.config/fish
git init
git remote add origin git@gitlab.com:sotilrac/fish_settings.git
git pull origin master
```
